# Nodejs Exercise

### Prerequisites

1. Create a MySQL database and name it `nodeexercise`.
2. Create a Table

```
CREATE TABLE users(
  id INT AUTO_INCREMENT,
  name VARCHAR(100),
  email VARCHAR(50),
  photo VARCHAR(100),
  PRIMARY KEY(id)
);
```

3. Insert this rows to the `users` table

```
INSERT INTO users (name, email, photo)
  values
  ('Edelgard', 'edelgard@mail.com', 'https://picsum.photos/200/300'),
  ('Dimitri', 'dimitri@mail.com', 'https://picsum.photos/200/300'),
  ('Claude', 'claude@mail.com', 'https://picsum.photos/200/300'),
  ('Lysithea', 'lysithea@mail.com', 'https://picsum.photos/200/300');
```

4. Create `.env` on the root folder and add the following variables, and replace `<your_credentials>`.

```
MYSQL_USER=<your_credentials>
MYSQL_PASSWORD=<your_credentials>
MYSQL_HOST=localhost
MYSQL_DB=nodeexercise
APP_URL=http://localhost:4000
```

### Running the App

1. For development, cd to root directory and run the command

```
npm run dev
```

The app should run now.

### Notes

If you have issues uploading a photo, please add `uploads` to the root folder.

### Running Tests

1. Cd to root directory and run the command

```
npm run test
```

## Author

**Clark Egbert Alinsob**
