const mysql = require('mysql')
const util = require('util')

const db = mysql.createPool({
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DB,
  connectionLimit: 10
})

db.getConnection((err, connection) => {
  if (err) throw new Error(err)

  connection.release()

  return
})

db.query = util.promisify(db.query)

module.exports = db
