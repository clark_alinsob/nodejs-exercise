const gql = require('graphql-tag')

const GET_USERS_QUERY = gql`
  query getUsers {
    getUsers {
      id
      name
      email
      photo
    }
  }
`

const GET_USER_QUERY = gql`
  query getUsers($id: Int!) {
    getUser(id: $id) {
      id
      name
      email
      photo
    }
  }
`

const UPDATE_USER_MUTATION = gql`
  mutation updateUser($id: Int!, $name: String!, $email: String!, $photo: String) {
    updateUser(id: $id, name: $name, email: $email, photo: $photo) {
      id
      name
      email
      photo
    }
  }
`

module.exports = {
  GET_USERS_QUERY,
  GET_USER_QUERY,
  UPDATE_USER_MUTATION
}
