const db = require('../db')

const resolvers = {
  // QUERIES *************
  getUsers: async () => {
    const users = await db.query('SELECT * FROM users')

    return users
  },

  getUser: async ({ id }) => {
    const result = await db.query('SELECT * FROM users WHERE id = ?', [id])
    const user = result[0]

    if (!user) throw new Error('User not found')

    return user
  },

  // MUTATIONS *************
  updateUser: async ({ id, name, email, photo }) => {
    if (name.trim() === '') throw new Error('Empty name')
    if (email.trim() === '') throw new Error('Empty email')

    const query = 'SELECT * FROM users WHERE id = ?'
    const result = await db.query(query, [id])
    const user = result[0]

    if (!user) throw new Error('User not found')
    if (!photo) photo = user.photo

    const updateQuery = 'UPDATE users SET name = ?, email = ?, photo = ?  WHERE id = ?'
    await db.query(updateQuery, [name, email, photo, id])

    return {
      id: user.id,
      name,
      email,
      photo
    }
  }
}

module.exports = resolvers
