const { buildSchema } = require('graphql')

const schema = buildSchema(`
  type User {
    id: Int!
    name: String!
    email: String!
    photo: String!
  }
  type Query {
    hello: String
    getUsers: [User]
    getUser(id: Int!): User
  }
  type Mutation {
    updateUser(id: Int!, name: String!, email: String!, photo: String): User
  }
`)

module.exports = schema
