const express = require('express')
const router = express.Router()
const multer = require('multer')
const fs = require('fs')
const userServices = require('../services/userServices')

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, `${__dirname}/../uploads`)
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1e9)}-${file.originalname}`
    cb(null, `${file.fieldname}-${uniqueSuffix}`)
  }
})

var upload = multer({ storage: storage })

router.get('/', async (req, res) => {
  const users = await userServices.getUsers()

  res.render('users-list', { users })
})

router.get('/:id', async (req, res) => {
  const user = await userServices.getUser(req.params.id)

  if (!user) throw new Error('User does not exist')

  res.render('profile', { user })
})

router.get('/:id/edit', async (req, res) => {
  const user = await userServices.getUser(req.params.id)

  if (!user) throw new Error('User does not exist')

  res.render('profile-edit', { user })
})

router.post('/:id/edit', upload.single('photo'), async (req, res) => {
  const photoUrl = req.file ? `${process.env.APP_URL}/uploads/${req.file.filename}` : null

  const user = await userServices.updateUser(req.params.id, req.body.name, req.body.email, photoUrl)

  if (user) res.redirect(`/users/${req.params.id}`)
})

module.exports = router
