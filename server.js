const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const exphbs = require('express-handlebars')
const { graphqlHTTP } = require('express-graphql')
const dotenv = require('dotenv')
dotenv.config()

// GraphQL Schema and Resolvers
const schema = require('./graphql/schema')
const rootValue = require('./graphql/resolvers')

// Routes Folder
const userRoutes = require('./routes/userRoutes')

const app = express()
const PORT = process.env.PORT || 4000

app.engine('handlebars', exphbs({ defaultLayout: 'main' }))
app.set('view engine', 'handlebars')

app.use(express.static(path.join(__dirname, 'public')))
app.use('/uploads', express.static('uploads'))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(
  '/graphql',
  graphqlHTTP({
    schema,
    rootValue,
    graphiql: true
  })
)

app.get('/', (req, res) => {
  res.render('home', { title: 'Node.js Exercise ' })
})
app.use('/users', userRoutes)
app.listen(PORT, () => console.log(`API listening on PORT ${PORT}`))
