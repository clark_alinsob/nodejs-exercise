fetch = require('node-fetch')
const { ApolloClient } = require('apollo-client')
const { HttpLink } = require('apollo-link-http')
const { InMemoryCache } = require('apollo-cache-inmemory')
const { GET_USERS_QUERY, GET_USER_QUERY, UPDATE_USER_MUTATION } = require('../graphql/gql')

const httpLink = new HttpLink({ uri: 'http://localhost:4000/graphql' })
const client = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache()
})

module.exports = {
  getUsers: () => {
    return new Promise((resolve, reject) => {
      client.query({ query: GET_USERS_QUERY }).then(res => {
        const users = res.data.getUsers

        resolve(users)
      })
    })
  },

  getUser: id => {
    return new Promise((resolve, reject) => {
      client
        .query({
          query: GET_USER_QUERY,
          variables: {
            id: parseInt(id)
          }
        })
        .then(res => {
          const user = res.data.getUser

          resolve(user)
        })
    })
  },

  updateUser: (id, name, email, photo) => {
    return new Promise((resolve, reject) => {
      client
        .mutate({
          mutation: UPDATE_USER_MUTATION,
          variables: {
            id: parseInt(id),
            name,
            email,
            photo
          }
        })
        .then(res => {
          const user = res.data.updateUser

          resolve(user)
        })
    })
  }
}
