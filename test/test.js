const assert = require('chai').assert
const userServices = require('../services/userServices')

testUserId = 1
testUserEmail = 'test@mail.com'
testUserName = 'Uchiha Sasuke'
testUserPhoto = 'https://picsum.photos/200/300'

describe('User Services', () => {
  describe('GET USERS', () => {
    it('GET multiple USERS should return an array', async () => {
      const results = await userServices.getUsers()
      assert.typeOf(results, 'array')
    })

    it('GET single USER should return an object', async () => {
      const results = await userServices.getUser(testUserId)
      assert.typeOf(results, 'object')
    })
  })

  describe('UPDATE USERS', () => {
    it('UPDATE single USER should return an object', async () => {
      const results = await userServices.updateUser(testUserId, testUserEmail, testUserName, testUserPhoto)
      assert.typeOf(results, 'object')
    })

    it('UPDATE single USER fields should not be empty', async () => {
      const results = await userServices.updateUser(testUserId, testUserEmail, testUserName, testUserPhoto)
      assert.typeOf(results, 'object')
    })
  })
})
